# WHUtils

[ ![Download](https://api.bintray.com/packages/mikewallacedev/maven/WHUtils/images/download.svg) ](https://bintray.com/mikewallacedev/maven/WHUtils/_latestVersion)

WHUtils is a collection of Android programming utilities that help you in your every day work.

## Components
Here are some detailed explanations on the different components that are in this package. They are :
- WHLog
- TimingLogger
- AcitivityUtils
- DeviceUtils
- KeyboardUtils
- PackageUtils
- ParcelableUtils
- Exceptions
- Password
- Utils
- ViewServer

### WHLog
A logger class that allows to easily define a logging level on a per tag basis. This allows you to split your logs and make them easier to read. It also allows you to define when a log will print.

You can set a log level per tag and the logger will only print out the logs that are of the set level or above for that tag.

For example, setting the log level at Log.ERROR will only print log of type error. Setting the log level at Log.INFO will print logs of type info, warning and error. Setting the log level at Log.VERBOSE will print all logs

#### Example
```java
 static {
   WHLog.LOG_LEVELS.put("ActivityMain", Log.ALL);  // Print all logs for this tag
   WHLog.LOG_LEVELS.put("OnCreate", Log.DEBUG);  // Print debug logs (and up) for this tag
   WHLog.LOG_LEVELS.put("OnPause", Log.NONE);  // Do not print logs for this tag
 }

 WHLog.w("ActivityMain", "Variable is null");  // Warning will print in the log
 WHLog.d("ActivityMain", "entering OnCreate"); // Debug will print in the log

 WHLog.v("OnCreate", "entering OnCreate");   // Verbose will NOT print in the log
 WHLog.d("OnCreate", "starting loop");       // Debug will print in the log
 WHLog.e("OnCreate", "Problem encountered"); // Error will print in the log

 WHLog.e("OnPause", "Problem encountered");   // Error will NOT print in the log
 WHLog.w("OnPause", "Problem encountered");   // Warning will NOT print in the log
```

### TimingLogger

A utility class to help log timings splits throughout a method call. Very helpful to quickly find bottle necks.

#### Example

```java
 TimingLogger timings = new TimingLogger(TAG, "methodA");
 // ... do some work A ...
 timings.addSplit("work A");
 // ... do some work B ...
 timings.addSplit("work B");
 // ... do some work C ...
 timings.addSplit("work C");
 timings.dumpToLog();
```
The dumpToLog call would add the following to the log:

```
D/TAG     ( 3459): methodA: begin
D/TAG     ( 3459): methodA:      9 ms, work A
D/TAG     ( 3459): methodA:      1 ms, work B
D/TAG     ( 3459): methodA:      6 ms, work C
D/TAG     ( 3459): methodA: end, 16 ms
```

### ActivityUtils
Simple utilities needed for the Activity class.

#### getActivity()
Will return the current parent Acitivity of an object. The object may be an Activity, an AndroidX Fragment or a base Android Fragment.

```java
public static Activity getActivity(@NonNull Object object)
```

### DeviceUtils
A collection of utility methods for getting device information.

#### getSecureAndroidIdLong()

```java
public static long getSecureAndroidIdLong(@NonNull final Context context)
```

#### getSecureAndroidIdHexString()

```java
public static String getSecureAndroidIdHexString(@NonNull final Context context)
```

### KeyboardUtils
Utilities that help with managing the Keyboard.


#### hideKeyboard()
Call this to make sure that the keyboard is not on screen.

```java
   public static void hideKeyboard(Activity activity)
```

### PackageUtils
Simple static helper class with utilities to get package details.

#### getAppsForIntent()
Gets a list of the apps that will respond to this Intent. Only filters that support the CATEGORY_DEFAULT will be considered for matching.

```java
public static List<ResolveInfo> getAppsForIntent(@NonNull final Context context, @NonNull final Intent intent)
```

#### hasAppsIntent()
Returns true if there is an app that will respond to the provided Intent. Only filters that support the CATEGORY_DEFAULT will be considered for matching.

```java
public static boolean hasAppsIntent(@NonNull final Context context, @NonNull final Intent intent)
```

#### getAppVersionName()
Get the app version name as defined in the Manifest (or Gradle build file).

```java
public static String getAppVersionName(Context context)
```

#### getPackageSignatureKeys()
Get the package signature keys.  Allows to compare keys from one package to another.
If any exceptions are thrown this method will catch them and return null.

```java
public static List<String> getPackageSignatureKeys(final Context context, final String packageName)
```

#### getPackageSignatureKeysOrThrow
Get the package signature keys.  Allows to compare keys from one package to another.

```java
public static List<String> getPackageSignatureKeysOrThrow(final Context context, final String packageName)
```


### ParcelableUtils
A set of methods that encode and decode data from/to a parcel object.

```java
public static void write(Parcel dest, String string)
public static String readString(Parcel source)

public static void write(Parcel dest, Parcelable parcelable, int flags)
public static <T extends Parcelable> T readParcelable(Parcel source)

public static void write(Parcel dest, Map<String, String> strings)
public static Map<String, String> readStringMap(Parcel source)

public static <T extends Parcelable> void write(Parcel dest, Map<String, T> objects, int flags)
public static <T extends Parcelable> Map<String, T> readParcelableMap(Parcel source)

public static void write(Parcel dest, URL url)
public static URL readURL(Parcel source)

public static void write(Parcel dest, Uri uri)
public static Uri readUri(Parcel source)

public static void write(Parcel dest, UUID uuid)
public static UUID readUUID(Parcel source)

public static void write(Parcel dest, DateTime dateTime)
public static DateTime readDateTime(Parcel source)

public static void write(Parcel dest, Instant instant)
public static Instant readInstant(Parcel source)

public static <T extends Enum<T>> void write(Parcel dest, Enum<T> enu)
public static <T extends Enum<T>> T readEnum(Parcel in, Class<T> clazz)

public static void write(Parcel dest, boolean bool)
public static boolean readBoolean(Parcel source)

public static <T extends Parcelable> void write(Parcel dest, List<T> fields, int flags)
public static <T extends Parcelable> List<T> readParcelableList(Parcel source)
```

### Exceptions
A collection of Exception classes that you can use in your code.
#### Exception_NotImplemented
Used while you are working on a component that has some unfinished parts. You are better off using this to remember that a portion of code is temporary rather than returning null.
#### Exception_Parsing
An exception to be thrown when some kind of parsing fails.
#### Exception_RequiredParameter
Thrown when a required parameter is absent or null
#### Exception_RestClient
Thrown when the REST client encounters an error
#### Exception_ThisShouldNeverHappen
  It should never happen. But it did.

### Password
Helper class to encode a password into a hash.

```java
public static String get_SHA_1_SecurePassword(String passwordToHash, String salt)
public static String get_SHA_256_SecurePassword(String passwordToHash, String salt)
public static String get_SHA_384_SecurePassword(String passwordToHash, String salt)
public static String get_SHA_512_SecurePassword(String passwordToHash, String salt)
```

### Utils
A collection of random utility methods
```java
int randInt(final int min, final int max)
boolean checkValidEmail(final CharSequence email)
String padDigitForDate(final int number)
String padNumber(final int number, final int desiredWidth, final char c, final boolean countSign)
int numDigits(final T number, final boolean countSign)
long convertHexToLong(String hexString)
```

### ViewServer
A copy of Romain Guy's ViewServer class. This class can be used to enable the use of HierarchyViewer inside an application. HierarchyViewer is an Android SDK tool that can be used to inspect and debug the user interface of running applications.

## How to use
#### Gradle

Add dependency in your build.gradle

*Note* : The actual latest version is
 [ ![Download](https://api.bintray.com/packages/mikewallacedev/maven/WHUtils/images/download.svg) ](https://bintray.com/mikewallacedev/maven/WHUtils/_latestVersion)
```groovy
implementation 'com.wallacehub.utils:WHUtils:0.0.0'
```
#### Maven
*Note* : The actual latest version is
 [ ![Download](https://api.bintray.com/packages/mikewallacedev/maven/WHUtils/images/download.svg) ](https://bintray.com/mikewallacedev/maven/WHUtils/_latestVersion)

```
<dependency>
	<groupId>com.wallacehub.utils</groupId>
	<artifactId>WHUtils</artifactId>
	<version>0.0.0</version>
	<type>pom</type>
</dependency>
```

## Contributions

Feel free to create issues and pull requests

## License

```
MIT License

Copyright for portions of RSAndroidOnboarder are held by Dzmitry Chyrta and Daniel Morales 2016, as part of project AndroidOnboarder.
All other copyright for RSAndroidOnboarder are held by Rise Software, 2016.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
