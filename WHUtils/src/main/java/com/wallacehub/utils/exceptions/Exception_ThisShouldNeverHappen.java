/*
 * The MIT License (MIT)
 *
 * Copyright (c)2020 Michael Wallace, Wallace Hub Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.utils.exceptions;

import androidx.annotation.NonNull;

/**
 * Thrown when this should never happen.
 *
 * @author :  Mike Wallace (+MikeWallaceDev) <mike@wallacehub.com> on 2017-02-17.
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class Exception_ThisShouldNeverHappen extends RuntimeException
{
	private static final long serialVersionUID = 6969655747795798325L;


	/**
	 * Constructs a new {@link Exception_ThisShouldNeverHappen} that includes the current stack trace.
	 */
	public Exception_ThisShouldNeverHappen() {
		super("This should never happen.");
	}


	/**
	 * Constructs a new {@link Exception_ThisShouldNeverHappen} that includes the current stack trace.
	 */
	public Exception_ThisShouldNeverHappen(@NonNull final String message) {
		super(message);
	}


	/**
	 * Constructs a new {@link Exception_ThisShouldNeverHappen} that includes
	 * the current stack trace and the specified cause.
	 *
	 * @param throwable the cause of this exception.
	 */
	public Exception_ThisShouldNeverHappen(final Throwable throwable) {
		super("This should never happen.", throwable);
	}
}
