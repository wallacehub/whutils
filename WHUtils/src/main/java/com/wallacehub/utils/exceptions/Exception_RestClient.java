/*
 * The MIT License (MIT)
 *
 * Copyright (c)2020 Michael Wallace, Wallace Hub Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * 2011 Foxykeep (http://datadroid.foxykeep.com)
 *
 * Licensed under the Beerware License :
 * 
 *   As long as you retain this notice you can do whatever you want with this stuff. If we meet some day, and you think
 *   this stuff is worth it, you can buy me a beer in return
 */
package com.wallacehub.utils.exceptions;

import android.util.Log;
import com.wallacehub.utils.logging.WHLog;

/**
 * Thrown when the REST client encounters an error
 *
 * @author :  Mike Wallace (+MikeWallaceDev) <mike@wallacehub.com> on 2015-02-22.
 */
@SuppressWarnings("unused")
public class Exception_RestClient extends Exception
{
	@SuppressWarnings({"FieldNameHidesFieldInSuperclass", "unused"})
	public static final String TAG = Exception_RestClient.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, Log.WARN);
	}

	private static final long serialVersionUID = 3544261729584965058L;

	private String m_newUrl;
	private int m_errorStatus = -1;


	/**
	 * Constructs a new {@link Exception_RestClient} that includes the current
	 * stack trace.
	 */
	public Exception_RestClient() {
		super();
	}


	/**
	 * Constructs a new {@link Exception_RestClient} that includes the current
	 * stack trace, the specified detail message and the specified cause.
	 *
	 * @param detailMessage the detail message for this exception.
	 * @param throwable     the cause of this exception.
	 */
	public Exception_RestClient(final String detailMessage, final Throwable throwable) {
		super(detailMessage, throwable);
	}


	/**
	 * Constructs a new {@link Exception_RestClient} that includes the current
	 * stack trace and the specified detail message.
	 *
	 * @param detailMessage the detail message for this exception.
	 */
	public Exception_RestClient(final String detailMessage) {
		super(detailMessage);
	}


	/**
	 * Constructs a new {@link Exception_RestClient} that includes the current
	 * stack trace and the specified detail message.
	 *
	 * @param detailMessage  the detail message for this exception.
	 * @param redirectionUrl url.
	 */
	public Exception_RestClient(final String detailMessage, final String redirectionUrl) {
		super(detailMessage);
		m_newUrl = redirectionUrl;
	}


	/**
	 * Constructs a new {@link Exception_RestClient} that includes the current
	 * stack trace and the specified detail message and the error status code
	 *
	 * @param detailMessage the detail message for this exception.
	 * @param errorStatus   The status received by the HTML call
	 */
	public Exception_RestClient(final String detailMessage, final int errorStatus) {
		super(detailMessage);
		m_errorStatus = errorStatus;
	}


	/**
	 * Constructs a new {@link Exception_RestClient} that includes the current
	 * stack trace and the specified cause.
	 *
	 * @param throwable the cause of this exception.
	 */
	public Exception_RestClient(final Throwable throwable) {
		super(throwable);
	}


	public String getRedirectionUrl() {
		return m_newUrl;
	}


	public int getErrorStatus() {
		return m_errorStatus;
	}
}
