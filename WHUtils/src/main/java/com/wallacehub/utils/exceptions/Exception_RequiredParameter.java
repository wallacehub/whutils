/*
 * The MIT License (MIT)
 *
 * Copyright (c)2020 Michael Wallace, Wallace Hub Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * 2011 Foxykeep (http://datadroid.foxykeep.com)
 *
 * Licensed under the Beerware License :
 * 
 *   As long as you retain this notice you can do whatever you want with this stuff. If we meet some day, and you think
 *   this stuff is worth it, you can buy me a beer in return
 */
package com.wallacehub.utils.exceptions;

/**
 * Thrown when a required parameter is absent or null
 *
 * @author :  Mike Wallace (+MikeWallaceDev) <mike@wallacehub.com> on 2016-02-22.
 */
@SuppressWarnings("unused")
public class Exception_RequiredParameter extends RuntimeException
{
	private static final long serialVersionUID = 473585379414167478L;


	/**
	 * Constructs a new Exception_RequiredParameter that includes the current stack trace.
	 */
	public Exception_RequiredParameter() {
		super();
	}


	/**
	 * Constructs a new Exception_RequiredParameter that includes the
	 * current stack trace, the specified detail message and the specified cause.
	 *
	 * @param detailMessage the detail message for this exception.
	 * @param throwable     the cause of this exception.
	 */
	public Exception_RequiredParameter(final String detailMessage, final Throwable throwable) {
		super(detailMessage, throwable);
	}


	/**
	 * Constructs a new Exception_RequiredParameter that includes the
	 * current stack trace and the specified detail message.
	 *
	 * @param detailMessage the detail message for this exception.
	 */
	public Exception_RequiredParameter(final String detailMessage) {
		super(detailMessage);
	}


	/**
	 * Constructs a new Exception_RequiredParameter that includes the
	 * current stack trace and the specified cause.
	 *
	 * @param throwable the cause of this exception.
	 */
	public Exception_RequiredParameter(final Throwable throwable) {
		super(throwable);
	}
}
