
/*
 * The MIT License (MIT)
 *
 * Copyright (c)2020 Michael Wallace, Wallace Hub Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.utils.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import com.wallacehub.utils.logging.WHLog;


/**
 * Simple class with network helpers
 *
 * @author :  Mike Wallace (+MikeWallaceDev) <mike@wallacehub.com> on 2016-02-22.
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class Network
{
	@SuppressWarnings({"FieldNameHidesFieldInSuperclass", "unused"})
	public static final String TAG = Network.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, Log.WARN);
	}

	public static final int CONNECTION_NONE   = 0;
	public static final int CONNECTED         = 1;
	public static final int CONNECTION_WIFI   = 2;
	public static final int CONNECTION_MOBILE = 4;


	// This is a utility class that cannot be instantiated.
	private Network() {
		super();
	}


	/**
	 * Returns the current network connection of the device.
	 * <p>
	 * If you use this method then you must add this line to your manifest <p>
	 * {@code <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>}
	 *
	 * @param context a context
	 * @return The state of the network connection. Either CONNECTION_NONE or a combination of :<br>
	 * CONNECTED<br>
	 * CONNECTION_WIFI<br>
	 * CONNECTION_MOBILE
	 */
	public static int hasNetworkConnection(final Context context) {
		int returnValue = CONNECTION_NONE;

		final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (null != connectivityManager) {
			final NetworkInfo[] netInfo = connectivityManager.getAllNetworkInfo();
			for (NetworkInfo ni : netInfo) {
				if (ni.isConnected()) {
					returnValue |= CONNECTED;

					if ("WIFI".equalsIgnoreCase(ni.getTypeName())) {
						returnValue |= CONNECTION_WIFI;
						continue;
					}

					if ("MOBILE".equalsIgnoreCase(ni.getTypeName())) {
						returnValue |= CONNECTION_MOBILE;
					}
				}
			}
		}
		return returnValue;
	}
}
