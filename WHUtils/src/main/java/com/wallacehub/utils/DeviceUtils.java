/*
 * The MIT License (MIT)
 *
 * Copyright (c)2020 Michael Wallace, Wallace Hub Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.utils;

import android.content.Context;
import android.provider.Settings;
import androidx.annotation.NonNull;
import com.wallacehub.utils.logging.WHLog;

import static com.wallacehub.utils.Utils.convertHexToLong;


/**
 * A collection of utility methods for getting device information
 *
 * @author :  Mike Wallace (+MikeWallaceDev) <mike@wallacehub.com> on 2017-01-06.
 */
@SuppressWarnings("WeakerAccess")
public class DeviceUtils
{
	private static final String TAG = DeviceUtils.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, WHLog.WARN);
	}


	/**
	 * Gets a 64-bit number (as a hex string) that is randomly generated when the user first
	 * sets up the device and should remain constant for the lifetime of the user's device.
	 * <p>
	 * It turns out that on a HTC One the Settings.Secure.ANDROID_ID string may be 15
	 * digits instead of 16!
	 *
	 * @param context
	 * @return The ID of the device as a long
	 */
	public static long getSecureAndroidIdLong(@NonNull final Context context) {
		return convertHexToLong(getSecureAndroidIdHexString(context));
	}


	/**
	 * Gets a 64-bit number (as a hex string) that is randomly generated when the user first
	 * sets up the device and should remain constant for the lifetime of the user's device.
	 * <p>
	 * It turns out that on a HTC One the Settings.Secure.ANDROID_ID string may be 15
	 * digits instead of 16!
	 *
	 * @param context
	 * @return The ID of the device as a String
	 */
	public static String getSecureAndroidIdHexString(@NonNull final Context context) {
		WHLog.d(TAG, "getSecureAndroidIdHexString");
		WHLog.d(TAG, "context = [ " + context + " ] \n");

		final String id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);

		WHLog.d(TAG, "Device id = " + id);
		return id;
	}
}
