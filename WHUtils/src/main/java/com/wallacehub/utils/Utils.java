/*
 * The MIT License (MIT)
 *
 * Copyright (c)2020 Michael Wallace, Wallace Hub Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.utils;

import com.wallacehub.utils.exceptions.Exception_RequiredParameter;
import com.wallacehub.utils.logging.WHLog;

import java.security.SecureRandom;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A collection of random utility methods
 *
 * @author :  Mike Wallace (+MikeWallaceDev) <mike@wallacehub.com> on 2015-02-25.
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class Utils
{
	@SuppressWarnings({"FieldNameHidesFieldInSuperclass", "unused"})
	public static final String TAG = Utils.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, WHLog.WARN);
	}

	@SuppressWarnings("HardcodedFileSeparator")
	public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	final static Random m_random = new SecureRandom();


	/**
	 * Returns a pseudo-random number between min and max, inclusive.
	 * The difference between min and max can be at most {@code Integer.MAX_VALUE - 1}.
	 *
	 * @param min Minimum value
	 * @param max Maximum value.  Must be greater than min.
	 * @return Integer between min and max, inclusive.
	 * @see Random#nextInt(int)
	 */
	public static int randInt(final int min, final int max) {

		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		return m_random.nextInt((max - min) + 1) + min;
	}


	/**
	 * A very basic email address validator
	 *
	 * @param email the email address to validate
	 * @return True if the email address is deemed valid
	 */
	public static boolean checkValidEmail(final CharSequence email) {
		if (null == email) {
			throw new Exception_RequiredParameter("email must not be null");
		}

		final Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		final Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}


	/**
	 * Helper that pads numbers to fill 2 columns
	 *
	 * @param number the number that requires padding
	 * @return string representation of the number with the required padding
	 */
	public static String padDigitForDate(final int number) {
		return padNumber(number, 2, '0', false);
	}


	/**
	 * Pads a number to a required width using a provided character as a filler.
	 *
	 * @param number       The number that you wish to pad
	 * @param desiredWidth The desired width of the number once padded
	 * @param c            The character to use as padding
	 * @param countSign    Should the minus sign of a negative number be counted
	 * @return string representation of the number with the required padding
	 */
	public static String padNumber(final int number, final int desiredWidth, final char c, final boolean countSign) {
		final StringBuilder strNumber = new StringBuilder(String.valueOf(number));
		final int missing = desiredWidth - numDigits(number, countSign);

		for (int i = 0; i < missing; i++) {
			strNumber.append(c);
		}

		return strNumber.toString();
	}


	/**
	 * Counts the number of digits in a number.  i.e.   100 =  3 digits.
	 *
	 * @param number    The number that you wish to count the digits of
	 * @param countSign Should the minus sign of a negative number be counted
	 * @return The number of digits in the number
	 */
	public static <T extends Number> int numDigits(final T number, final boolean countSign) {
		int digits = 0;
		double value = number.doubleValue();

		// remove this line if '-' counts as a digit
		if (countSign && (0.0 > value)) {
			digits = 1;
		}

		while (0.0 < value) {
			value /= 10.0;
			digits++;
		}

		return digits;
	}



	/**
	 * Method to convert a 16-character hex string into a Java long.  there is a known bug in Java that
	 * took them 13 years to fix, and still isn't fixed in the version of Java used for Android development.
	 * http://bugs.java.com/bugdatabase/view_bug.do?bug_id=4215269
	 * http://stackoverflow.com/questions/1410168/how-to-parse-negative-long-in-hex-in-java
	 *
	 * @param hexString the hex string to convert.
	 * @return 0 if there is an error
	 */
	public static long convertHexToLong(String hexString) throws NumberFormatException {

		hexString = "0000000000000000" + hexString;
		final int i = hexString.length();
		hexString = hexString.substring(i - 16, i);

		return Long.parseLong(hexString.substring(0, 8), 16) << 32 | Long.parseLong(hexString.substring(8, 16), 16);
	}
}
