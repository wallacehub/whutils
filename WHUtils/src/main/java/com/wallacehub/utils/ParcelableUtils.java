/*
 * The MIT License (MIT)
 *
 * Copyright (c)2020 Michael Wallace, Wallace Hub Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.utils;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.wallacehub.utils.logging.WHLog;
import org.joda.time.DateTime;
import org.joda.time.Instant;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * A set of methods that encode and decode data from/to a parcel object.
 *
 * @author :  https://gist.github.com/keyboardr/5563038
 * @author :  Mike Wallace (+MikeWallaceDev) <mike@wallacehub.com> on 2016-02-22.
 */
@SuppressWarnings("unused")
public class ParcelableUtils
{
	@SuppressWarnings({"FieldNameHidesFieldInSuperclass", "unused"})
	public static final String TAG = ParcelableUtils.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, Log.WARN);
	}

	public static void write(Parcel dest, String string) {
		dest.writeByte((byte) (string == null ? 0 : 1));
		if (string != null) {
			dest.writeString(string);
		}
	}


	public static String readString(Parcel source) {
		if (source.readByte() == 1) {
			return source.readString();
		}
		return null;
	}


	public static void write(Parcel dest, Parcelable parcelable, int flags) {
		dest.writeByte((byte) (parcelable == null ? 0 : 1));
		if (parcelable != null) {
			dest.writeParcelable(parcelable, flags);
		}
	}


	public static <T extends Parcelable> T readParcelable(Parcel source) {
		if (source.readByte() == 1) {
			return source.readParcelable(Thread.currentThread().getContextClassLoader());
		}
		return null;
	}


	public static void write(Parcel dest, Map<String, String> strings) {
		if (strings == null) {
			dest.writeInt(-1);
		}
		{
			dest.writeInt(strings.keySet().size());
			for (String key : strings.keySet()) {
				dest.writeString(key);
				dest.writeString(strings.get(key));
			}
		}
	}


	public static Map<String, String> readStringMap(Parcel source) {
		int numKeys = source.readInt();
		if (numKeys == -1) {
			return null;
		}
		HashMap<String, String> map = new HashMap<>();
		for (int i = 0; i < numKeys; i++) {
			String key = source.readString();
			String value = source.readString();
			map.put(key, value);
		}
		return map;
	}


	public static <T extends Parcelable> void write(Parcel dest, Map<String, T> objects, int flags) {
		if (objects == null) {
			dest.writeInt(-1);
		}
		else {
			dest.writeInt(objects.keySet().size());
			for (String key : objects.keySet()) {
				dest.writeString(key);
				dest.writeParcelable(objects.get(key), flags);
			}
		}
	}


	public static <T extends Parcelable> Map<String, T> readParcelableMap(
			Parcel source) {
		int numKeys = source.readInt();
		if (numKeys == -1) {
			return null;
		}
		HashMap<String, T> map = new HashMap<>();
		for (int i = 0; i < numKeys; i++) {
			String key = source.readString();
			T value = source.readParcelable(Thread.currentThread().getContextClassLoader());
			map.put(key, value);
		}
		return map;
	}


	public static void write(Parcel dest, URL url) {
		dest.writeString(url.toExternalForm());
	}


	public static URL readURL(Parcel source) {
		try {
			return new URL(source.readString());
		}
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}


	public static void write(Parcel dest, Uri uri) {
		dest.writeByte((byte) (uri == null ? 0 : 1));
		if (null != uri) {
			dest.writeString(uri.toString());
		}
	}


	public static Uri readUri(Parcel source) {
		if (source.readByte() == 1) {
			return Uri.parse(source.readString());
		}

		return null;
	}


	/**
	 * UUID
	 */
	public static void write(Parcel dest, UUID uuid) {
		dest.writeByte((byte) (uuid == null ? 0 : 1));
		if (null != uuid) {
			dest.writeString(uuid.toString());
		}
	}


	public static UUID readUUID(Parcel source) {
		if (source.readByte() == 1) {
			return UUID.fromString(source.readString());
		}

		return null;
	}


	public static void write(Parcel dest, DateTime dateTime) {
		dest.writeByte((byte) (dateTime == null ? 0 : 1));
		if (dateTime != null) {
			dest.writeLong(dateTime.getMillis());
		}
	}


	public static DateTime readDateTime(Parcel source) {
		if (source.readByte() == 1) {
			return new DateTime(source.readLong());
		}
		return null;
	}


	public static void write(Parcel dest, Instant instant) {
		dest.writeByte((byte) (instant == null ? 0 : 1));
		if (instant != null) {
			dest.writeLong(instant.getMillis());
		}
	}


	public static Instant readInstant(Parcel source) {
		if (source.readByte() == 1) {
			return new Instant(source.readLong());
		}
		return null;
	}


	public static <T extends Enum<T>> void write(Parcel dest, Enum<T> enu) {
		if (enu == null) {
			dest.writeString("");
		}
		else {
			dest.writeString(enu.name());
		}
	}


	public static <T extends Enum<T>> T readEnum(Parcel in, Class<T> clazz) {
		String name = in.readString();
		if ("".equals(name)) {
			return null;
		}
		return Enum.valueOf(clazz, name);
	}


	public static void write(Parcel dest, boolean bool) {
		dest.writeByte((byte) (bool ? 1 : 0));
	}


	public static boolean readBoolean(Parcel source) {
		return source.readByte() == 1;
	}


	public static <T extends Parcelable> void write(Parcel dest,
																	List<T> fields, int flags) {
		if (fields == null) {
			dest.writeInt(-1);
		}
		else {
			dest.writeInt(fields.size());
			for (T field : fields) {
				dest.writeParcelable(field, flags);
			}
		}
	}


	@SuppressWarnings("unchecked")
	public static <T extends Parcelable> List<T> readParcelableList(
			Parcel source) {
		int size = source.readInt();
		if (size == -1) {
			return null;
		}
		ArrayList<T> list = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			list.add((T) source.readParcelable(Thread.currentThread().getContextClassLoader()));
		}
		return list;
	}
}
