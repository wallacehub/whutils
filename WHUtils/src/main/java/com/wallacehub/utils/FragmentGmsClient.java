/*
 * The MIT License (MIT)
 *
 * Copyright (c)2020 Michael Wallace, Wallace Hub Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.utils;

import android.app.Fragment;

/**
 * @author :  Mike Wallace (+MikeWallaceDev) <mike@wallacehub.com> on 2015-06-10.
 */
@SuppressWarnings("ALL")
public class FragmentGmsClient extends Fragment //implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
{
//	private static final String TAG_FRAG = FragmentGmsClient.class.getSimpleName();
//
//	static {
//		WHLog.LOG_LEVELS.put(TAG_FRAG, WHLog.ERROR);
//	}
//
//	// Constants
//	// Request code to use when launching the resolution activity
//	private static final int    REQUEST_RESOLVE_ERROR = 1001;
//	// Unique tag for the error dialog fragment
//	private static final String DIALOG_ERROR          = "dialog_error";
//
//	// Data
//	private GoogleApiClient m_googleApiClient = null;
//
//	private boolean m_resolvingError = false; // Bool to track whether the app is already resolving an error
//
//
//	public interface GmsClient
//	{
//		void onConnected(GoogleApiClient m_googleApiClient, final Bundle bundle);
//
//		void onConnectionSuspended(final int i);
//
//		void onConnectionFailed(final ConnectionResult connectionResult);
//	}
//
//
////	public static <F extends Fragment & GmsClient> FragmentGmsClient attach(final F parent) {
////		return attach(parent.getChildFragmentManager());
////	}
//
//
//	public static <A extends Activity & GmsClient> FragmentGmsClient attach(final A parent) {
//		return attach(parent.getFragmentManager());
//	}
//
//
//	private static FragmentGmsClient attach(final FragmentManager fragmentManager) {
//		FragmentGmsClient frag = (FragmentGmsClient) fragmentManager.findFragmentByTag(TAG_FRAG);
//
//		if (null == frag) {
//			frag = new FragmentGmsClient();
//			fragmentManager.beginTransaction().add(frag, TAG_FRAG).commit();
//		}
//		return frag;
//	}
//
//
//	@Override
//	public void onActivityCreated(Bundle savedInstanceState) {
//		super.onActivityCreated(savedInstanceState);
//
//		if (!isGooglePlayServicesAvailable()) {
//			WHLog.e(TAG_FRAG, "Google Play services unavailable.");
//			getActivity().finish();
//			return;
//		}
//
//		m_googleApiClient = new GoogleApiClient.Builder(getActivity())
//				.addApi(ActivityRecognition.API)
//				.addApi(LocationServices.API)
//				.addApi(Places.GEO_DATA_API)
//				.addApi(Places.PLACE_DETECTION_API)
//				.addConnectionCallbacks(this)
//				.addOnConnectionFailedListener(this)
//				.build();
//	}
//
//
//	@Override
//	public void onStart() {
//		super.onStart();
//
//		if (!m_resolvingError) {
//			m_googleApiClient.connect();
//		}
//	}
//
//
//	@Override
//	public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
//		if (REQUEST_RESOLVE_ERROR == requestCode) {
//			m_resolvingError = false;
//			if (Activity.RESULT_OK == resultCode) {
//				// Make sure the app is not already connected or attempting to connect
//				if (!m_googleApiClient.isConnecting() && !m_googleApiClient.isConnected()) {
//					m_googleApiClient.connect();
//				}
//			}
//		}
//	}
//
//
//	@Override
//	public void onStop() {
//		m_googleApiClient.disconnect();
//
//		super.onStop();
//	}
//
//
//	/**
//	 * Checks if Google Play services is available.
//	 *
//	 * @return true if it is.
//	 */
//	private boolean isGooglePlayServicesAvailable() {
//		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
//		if (ConnectionResult.SUCCESS == resultCode) {
//			WHLog.d(TAG_FRAG, "Google Play services is available.");
//			return true;
//		}
//		else {
//			WHLog.e(TAG_FRAG, "Google Play services is unavailable.");
//			return false;
//		}
//	}
//
//
//	@Override
//	public void onConnected(final Bundle bundle) {
//		WHLog.v(TAG_FRAG, "onConnected");
//
//		getParent().onConnected(m_googleApiClient, bundle);
//	}
//
//
//	@Override
//	public void onConnectionSuspended(final int i) {
//		WHLog.d(TAG_FRAG, "onConnectionSuspended");
//
//		getParent().onConnectionSuspended(i);
//	}
//
//
//	@Override
//	public void onConnectionFailed(@NonNull final ConnectionResult connectionResult) {
//		WHLog.e(TAG_FRAG, String.format(Locale.getDefault(), "onConnectionFailed, connectionResult = %d ", connectionResult.getErrorCode()));
//
//		getParent().onConnectionFailed(connectionResult);
//
//		if (m_resolvingError) {
//			// Already attempting to resolve an error.
//			return;
//		}
//		else if (connectionResult.hasResolution()) {
//			try {
//				m_resolvingError = true;
//				connectionResult.startResolutionForResult(getActivity(), REQUEST_RESOLVE_ERROR);
//			}
//			catch (final IntentSender.SendIntentException e) {
//				WHLog.e(TAG_FRAG, e.toString(), e);  // NON-NLS
//
//				// There was an error with the resolution intent. Try again.
//				m_googleApiClient.connect();
//			}
//		}
//		else {
//			// Show dialog using GooglePlayServicesUtil.getErrorDialog()
//			showErrorDialog(connectionResult.getErrorCode());
//			m_resolvingError = true;
//		}
//	}
//
//
//	private GmsClient getParent() {
////		final Fragment parentFragment = getParentFragment();
////		if (parentFragment instanceof GmsClient) {
////			return (GmsClient) parentFragment;
////		}
////		else {
//		final Activity activity = getActivity();
//		if (activity instanceof GmsClient) {
//			return (GmsClient) activity;
//		}
////		}
//		return null;
//	}
//
//
//	/* Creates a dialog for an error message */
//	private void showErrorDialog(final int errorCode) {
//		// Create a fragment for the error dialog
//		final ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
//
//		// Pass the error that should be displayed
//		final Bundle args = new Bundle();
//		args.putInt(DIALOG_ERROR, errorCode);
//		dialogFragment.setArguments(args);
//		dialogFragment.show(getFragmentManager(), "errordialog");
//	}
//
//
//	/* Called from ErrorDialogFragment when the dialog is dismissed. */
//	public void onDialogDismissed() {
//		m_resolvingError = false;
//	}
//
//
//	/* A fragment to display an error dialog */
//	public static class ErrorDialogFragment extends DialogFragment
//	{
//		public ErrorDialogFragment() { }
//
//
//		@SuppressWarnings("RefusedBequest")
//		@Override
//		public Dialog onCreateDialog(final Bundle savedInstanceState) {
//			// Get the error code and retrieve the appropriate dialog
//			final int errorCode = this.getArguments().getInt(DIALOG_ERROR);
//			return GooglePlayServicesUtil.getErrorDialog(errorCode, this.getActivity(), REQUEST_RESOLVE_ERROR);
//		}
//
//
////		@Override
////		public void onDismiss(final DialogInterface dialog) {
////			(getActivity()).onDialogDismissed();
////		}
//	}
}
