/*
 * The MIT License (MIT)
 *
 * Copyright (c)2020 Michael Wallace, Wallace Hub Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import androidx.annotation.NonNull;
import android.util.Log;
import com.wallacehub.utils.exceptions.Exception_RequiredParameter;
import com.wallacehub.utils.logging.WHLog;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

/**
 * Simple helper class with utilities to get package details
 *
 * @author :  Mike Wallace (+MikeWallaceDev) <mike@wallacehub.com> on 2015-03-10.
 */
@SuppressWarnings("unused")
public final class PackageUtils
{
	@SuppressWarnings({"FieldNameHidesFieldInSuperclass", "unused"})
	public static final String TAG = PackageUtils.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, Log.WARN);
	}

	private static final String MESSAGE_DIGEST_ALGORITHM = "SHA1";
	private static final String CERTIFICATE_FACTORY      = "X509";
	private static final char   SHA_SEPARATOR            = ':';
	private static final char   LEADING_ZERO             = '0';


	/**
	 * Gets a list of the apps that will respond to this Intent.
	 * Only filters that support the CATEGORY_DEFAULT will be considered for matching.
	 *
	 * @param context The current context
	 * @param intent The Intent that you want to test
	 * @return A List of ResolveInfo objects containing one entry for each matching activity, ordered from best to worst.
	 */
	public static List<ResolveInfo> getAppsForIntent(@NonNull final Context context, @NonNull final Intent intent) {
		final PackageManager packageManager = context.getPackageManager();

		return packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
	}


	/**
	 * Returns true if there is an app that will respond to the provided Intent
	 * Only filters that support the CATEGORY_DEFAULT will be considered for matching.
	 *
	 * @param context The current context
	 * @param intent The Intent that you want to test
	 * @return A List of ResolveInfo objects containing one entry for each matching activity, ordered from best to worst.
	 */
	public static boolean hasAppsIntent(@NonNull final Context context, @NonNull final Intent intent) {
		final List<ResolveInfo> appsForIntent = getAppsForIntent(context, intent);
		return (null != appsForIntent) && (appsForIntent.size() > 0);
	}


	// This is a utility class that cannot be instantiated.
	private PackageUtils() {
		super();
	}


	/**
	 * Get the app version name as defined in the Manifest (or Gradle build file)
	 *
	 * @param context a context
	 * @return The app version name
	 */
	public static String getAppVersionName(Context context) {
		if (null == context) {
			throw new Exception_RequiredParameter("context parameter must not be null");
		}

		try {
			final String packageName = context.getPackageName();

			final PackageManager packageManager = context.getPackageManager();
			final PackageInfo packageInfo = packageManager.getPackageInfo(packageName, 0);

			return packageInfo.versionName;
		}
		catch (PackageManager.NameNotFoundException e) {
			WHLog.e(context.toString(), e.getMessage(), e);
		}

		return null;
	}


	/**
	 * Get the package signature keys.  Allows to compare keys from one package to another.
	 * If any exceptions are thrown this method will catch them and return null.
	 *
	 * @param context     a context
	 * @param packageName the name of the package whose keys you want to view.
	 * @return a list of keys or null if an exception was thrown
	 */
	public static List<String> getPackageSignatureKeys(final Context context, final String packageName) {
		try {
			return getPackageSignatureKeysOrThrow(context, packageName);
		}
		catch (final PackageManager.NameNotFoundException | NoSuchAlgorithmException | CertificateException e) {
			Log.e(TAG, e.toString(), e);
		}
		return null;
	}


	/**
	 * Get the package signature keys.  Allows to compare keys from one package to another
	 *
	 * @param context     a context
	 * @param packageName the name of the package whose keys you want to view.
	 * @return a list of keys
	 */
	public static List<String> getPackageSignatureKeysOrThrow(final Context context, final String packageName)
	throws PackageManager.NameNotFoundException, CertificateException, NoSuchAlgorithmException {
		final List<String> hashKeys = new ArrayList<>(5);
		final PackageManager packageManager = context.getPackageManager();

		PackageInfo packageInfo = packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES);

		CertificateFactory certificateFactory = CertificateFactory.getInstance(CERTIFICATE_FACTORY);

		MessageDigest messageDigest = MessageDigest.getInstance(MESSAGE_DIGEST_ALGORITHM);

		if ((null == packageInfo) || (null == certificateFactory) || (null == messageDigest)) {
			return null;
		}

		for (final Signature signature : packageInfo.signatures) {
			final byte[] cert = signature.toByteArray();
			final InputStream input = new ByteArrayInputStream(cert);

			//noinspection TryWithIdenticalCatches
			final X509Certificate certificate = (X509Certificate) certificateFactory.generateCertificate(input);

			final byte[] publicKey = messageDigest.digest(certificate.getEncoded());
			messageDigest.reset();

			hashKeys.add(byte2HexFormattedSHA(publicKey));
		}

		return hashKeys;
	}


	@SuppressWarnings({"StringContatenationInLoop", "StringConcatenationMissingWhitespace"})
	public static String byte2HexFormattedSHA(final byte[] arr) {
		final StringBuilder stringBuilder = new StringBuilder(arr.length * 2);
		final int arrLength = arr.length;

		for (int i = 0; i < arrLength; i++) {
			String hexString = Integer.toHexString(arr[i]);
			final int length = hexString.length();
			if (1 == length) {
				hexString = LEADING_ZERO + hexString;
			}

			if (2 < length) {
				hexString = hexString.substring(length - 2, length);
			}

			stringBuilder.append(hexString.toUpperCase());

			if (i < (arr.length - 1)) {
				stringBuilder.append(SHA_SEPARATOR);
			}
		}

		return stringBuilder.toString();
	}
}
