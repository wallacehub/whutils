/*
 * The MIT License (MIT)
 *
 * Copyright (c)2020 Michael Wallace, Wallace Hub Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.utils.logging;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;


/**
 * A logger class that allows to easily define a logging level on a per tag basis.
 * You can set a log level in a class and the logger will only print out the logs
 * that are of the set level or above.
 * <p/>
 * For example, setting the log level at Log.ERROR will only print log of type error.
 * Setting the log level at Log.INFO will print logs of type info, warning and error.
 * Setting the log level at Log.VERBOSE will print all logs.
 * <p/>
 * static {
 * WHLog.LOG_LEVELS.put(TAG, Log.ERROR);
 * }
 *
 * @author :  Mike Wallace (+MikeWallaceDev) <mike@wallacehub.com> on 2016-02-22.
 */
@SuppressWarnings({"StaticMethodNamingConvention", "unused", "WeakerAccess"})
public class WHLog
{
	@SuppressWarnings("unused")
	public static final int     ASSERT  = 7;
	public static final Integer ERROR   = Log.ERROR;  // 6
	public static final Integer WARN    = Log.WARN;   // 5
	public static final Integer INFO    = Log.INFO; // 4
	public static final Integer DEBUG   = Log.DEBUG; // 3
	public static final Integer VERBOSE = Log.VERBOSE;  // 2


	public static final Map<String, Integer> LOG_LEVELS = new HashMap<>(5);


	public static int getLogLevel(final String tag) {
		if (LOG_LEVELS.containsKey(tag)) {
			return LOG_LEVELS.get(tag);
		}
		else {
			return Log.VERBOSE;
		}
	}


	public static void e(final String tag, final String msg) {
		if (getLogLevel(tag) <= WHLog.ERROR) {
			Log.e(tag, msg);
		}
	}


	public static void e(final String tag, final String msg, final Throwable throwable) {
		if (getLogLevel(tag) <= WHLog.ERROR) {
			Log.e(tag, msg, throwable);
		}
	}


	public static void w(final String tag, final String msg) {
		if (getLogLevel(tag) <= WHLog.WARN) {
			Log.w(tag, msg);
		}
	}


	public static void w(final String tag, final String msg, final Throwable throwable) {
		if (getLogLevel(tag) <= WHLog.WARN) {
			Log.w(tag, msg, throwable);
		}
	}


	public static void i(final String tag, final String msg) {
		if (getLogLevel(tag) <= WHLog.INFO) {
			Log.i(tag, msg);
		}
	}


	public static void i(final String tag, final String msg, final Throwable throwable) {
		if (getLogLevel(tag) <= WHLog.INFO) {
			Log.i(tag, msg, throwable);
		}
	}


	public static void d(final String tag, final String msg) {
		if (getLogLevel(tag) <= WHLog.DEBUG) {
			Log.d(tag, msg);
		}
	}


	public static void d(final String tag, final String msg, final Throwable throwable) {
		if (getLogLevel(tag) <= WHLog.DEBUG) {
			Log.d(tag, msg, throwable);
		}
	}


	public static void v(final String tag, final String msg) {
		if (getLogLevel(tag) <= WHLog.VERBOSE) {
			Log.v(tag, msg);
		}
	}


	public static void v(final String tag, final String msg, final Throwable throwable) {
		if (getLogLevel(tag) <= WHLog.VERBOSE) {
			Log.v(tag, msg, throwable);
		}
	}
}
