/*
 * The MIT License (MIT)
 *
 * Copyright (c)2020 Michael Wallace, Wallace Hub Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.utils.logging;

import org.joda.time.Instant;

import java.util.ArrayList;

/**
 * A utility class to help log timings splits throughout a method call.
 * Typical usage is:
 * <p/>
 * <pre>
 *     TimingLogger timings = new TimingLogger(TAG, "methodA");
 *     // ... do some work A ...
 *     timings.addSplit("work A");
 *     // ... do some work B ...
 *     timings.addSplit("work B");
 *     // ... do some work C ...
 *     timings.addSplit("work C");
 *     timings.dumpToLog();
 * </pre>
 * <p/>
 * <p>The dumpToLog call would add the following to the log:</p>
 * <p/>
 * <pre>
 *     D/TAG     ( 3459): methodA: begin
 *     D/TAG     ( 3459): methodA:      9 ms, work A
 *     D/TAG     ( 3459): methodA:      1 ms, work B
 *     D/TAG     ( 3459): methodA:      6 ms, work C
 *     D/TAG     ( 3459): methodA: end, 16 ms
 * </pre>
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class TimingLogger
{
	private class Split
	{
		Instant splitTime;
		String  splitLabel;


		Split(Instant splitTime, String splitLabel) {
			this.splitTime = splitTime;
			this.splitLabel = splitLabel;
		}
	}


	/**
	 * The Log tag to use for logging the timings.
	 * This would usually be the name of your class.
	 */
	private String m_tag;

	/**
	 * A label to be included in every log.
	 * This would be either a method name or the "type" of calculation your are doing
	 */
	private String m_label;

	/**
	 * Stores the time of each split.
	 */
	private ArrayList<Split> m_splits;


	/**
	 * Create and initialize a TimingLogger object that will log using the specific tag and label.
	 * The tag would usually be the name of your class.
	 * The label would be either a method name or the "type" of calculation your are doing
	 *
	 * @param tag   the log tag to use while logging the timings
	 * @param label a string to be displayed with each log
	 */
	public TimingLogger(final String tag, final String label) {
		m_splits = new ArrayList<>();
		m_tag = tag;
		m_label = label;

		reset();
	}


	/**
	 * Clear a TimingLogger object.
	 */
	public void reset() {
		m_splits.clear();

		addSplit(null);
	}


	/**
	 * Add a split for the current time, labeled with splitLabel.
	 *
	 * @param splitLabel a label to associate with this split.
	 */
	public void addSplit(final String splitLabel) {
		m_splits.add(new Split(Instant.now(), splitLabel));
	}


	/**
	 * Dumps the timings to the log using Log.d().
	 * Each time is displayed as an interval in ms
	 */
	public void dumpAsIntervals() {
		WHLog.d(m_tag, m_label + ": begin");

		final Split first = m_splits.get(0);
		Split current = first;
		Split prev = first;

		for (int i = 1; i < m_splits.size(); i++) {
			current = m_splits.get(i);
			WHLog.d(m_tag, m_label + ":      " + current.splitTime.minus(prev.splitTime.getMillis()).getMillis() + " ms, " + current.splitLabel);
			prev = current;
		}

		WHLog.d(m_tag, m_label + ": end, total : " + current.splitTime.minus(first.splitTime.getMillis()).getMillis() + " ms");
	}


	/**
	 * Dumps the timings to the log using Log.d().
	 * Each time is displayed as the actual time the event happened.
	 */
	public void dumpAsTimes() {
		WHLog.d(m_tag, m_label + ": begin");
		final Split first = m_splits.get(0);
		Split current = first;

		for (int i = 0; i < m_splits.size(); i++) {
			current = m_splits.get(i);
			WHLog.d(m_tag, m_label + ":      " + current.splitTime + " ms, " + current.splitLabel);
		}

		WHLog.d(m_tag, m_label + ": end, total : " + current.splitTime.minus(first.splitTime.getMillis()).getMillis() + " ms");
	}


	/**
	 * Dumps the timings to the log using WHLog.d().
	 */
	public void splitAndDumpAsIntervals(final String splitLabel) {
		addSplit(splitLabel);
		dumpAsIntervals();
	}


	/**
	 * Dumps the timings to the log using WHLog.d().
	 */
	public void splitAndDumpAsTimes(final String splitLabel) {
		addSplit(splitLabel);
		dumpAsTimes();
	}
}
