/*
 * The MIT License (MIT)
 *
 * Copyright (c)2020 Michael Wallace, Wallace Hub Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.testapplication;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;

public class Activity_GooglePlayServices extends AppCompatActivity //implements FragmentGmsClient.GmsClient
{
	public static void startActivity(final Context context) {
		Intent intent = new Intent(context, Activity_GooglePlayServices.class);
		context.startActivity(intent);
	}


//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_google_play_services);
//
//		FragmentGmsClient.attach(this);
//	}
//
//
//	@Override
//	public void onConnected(GoogleApiClient m_googleApiClient, Bundle bundle) {
//		Toast.makeText(this, "OnConnected", Toast.LENGTH_SHORT).show();
//	}
//
//
//	@Override
//	public void onConnectionSuspended(int i) {
//		Toast.makeText(this, "onConnectionSuspended", Toast.LENGTH_SHORT).show();
//	}
//
//
//	@Override
//	public void onConnectionFailed(ConnectionResult connectionResult) {
//		Toast.makeText(this, "onConnectionFailed", Toast.LENGTH_SHORT).show();
//	}
}
